#include <string>
#include "GL/glew.h"
#include "stb_image.h"
#include <iostream>
using namespace std;

namespace tff {
	unsigned int TextureFromFile(const char *path, const string &directory, bool gamma = false);
};